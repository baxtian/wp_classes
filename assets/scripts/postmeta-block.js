var postmetaBlock = function(json) {
	var registerBlockType = window.wp.blocks.registerBlockType;
	var el = window.wp.element.createElement;
	var __ = window.wp.i18n.__;
	var TextControl = window.wp.components.TextControl;
	var CheckboxControl = window.wp.components.CheckboxControl;
	var withSelect = window.wp.data.withSelect;
	var withDispatch = window.wp.data.withDispatch;
	var compose = window.wp.compose.compose;

	var data = JSON.parse(json);

	var MetaTextControl = compose(withDispatch(function(dispatch, props) {
		return {
			setMetaValue: function(metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function(select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function(props) {
		return el(TextControl, {
			label: props.title,
			value: props.metaValue,
			onChange: function(content) {
				props.setMetaValue(content);
			}
		});
	});

	var MetaTextareaControl = compose(withDispatch(function(dispatch, props) {
		return {
			setMetaValue: function(metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function(select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function(props) {
		return el(TextareaControl, {
			label: props.title,
			value: props.metaValue,
			onChange: function(content) {
				props.setMetaValue(content);
			}
		});
	});

	var MetaCheckboxControl = compose(withDispatch(function(dispatch, props) {
		return {
			setMetaValue: function(isChecked) {
				dispatch('core/editor').editPost({
					[props.metaKey]: isChecked
				});
			}
		}
	}), withSelect(function(select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function(props) {
		return el(CheckboxControl, {
			label: props.title,
			checked: props.metaValue,
			onChange: function(isChecked) {
				props.setMetaValue(isChecked);
			}
		});
	});

	registerBlockType(data.prefix + '/' + data.name, {
		title: data.title,
		icon: 'media-code',
		category: 'layout',
		keywords: data.keywords,
		supports: {
			inserter: false, //Este plugin está pensado para ser usado en un templade de una estructura
			reusable: false,
			html: false
		},

		edit: function(props) {
			var className = props.className;
			var setAttributes = props.setAttributes;

			var controls = data.vars.map(function(item) {
				if (item.type == 'checkbox') {
					return el(MetaCheckboxControl, {
						metaKey: item.name,
						title: item.help
					});
				} else {
					return el(MetaTextControl, {
						metaKey: item.name,
						title: item.label
					});
				}
			});

			return el('div', {
				className: className
			}, controls);
		},

		// No information saved to the block
		// Data is saved to post meta via attributes
		save: function() {
			return null;
		},
	});
}
