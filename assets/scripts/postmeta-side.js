var postmetaSide = function(json) {
	var registerPlugin = window.wp.plugins.registerPlugin;
	var PluginDocumentSettingPanel = window.wp.editPost.PluginDocumentSettingPanel;
	var __ = window.wp.i18n.__;
	var el = window.wp.element.createElement;
	var TextControl = window.wp.components.TextControl;
	var TextareaControl = window.wp.components.TextareaControl;
	var CheckboxControl = window.wp.components.CheckboxControl;
	var withSelect = window.wp.data.withSelect;
	var withDispatch = window.wp.data.withDispatch;
	var compose = window.wp.compose.compose;

	var data = JSON.parse(json);

	var MetaTextControl = compose(withDispatch(function(dispatch, props) {
		return {
			setMetaValue: function(metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function(select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function(props) {
		return el(TextControl, {
			label: props.title,
			value: props.metaValue,
			onChange: function(content) {
				props.setMetaValue(content);
			}
		});
	});

	var MetaTextareaControl = compose(withDispatch(function(dispatch, props) {
		return {
			setMetaValue: function(metaValue) {
				dispatch('core/editor').editPost({
					[props.metaKey]: metaValue
				});
			}
		}
	}), withSelect(function(select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function(props) {
		return el(TextareaControl, {
			label: props.title,
			value: props.metaValue,
			onChange: function(content) {
				props.setMetaValue(content);
			}
		});
	});

	var MetaCheckboxControl = compose(withDispatch(function(dispatch, props) {
		return {
			setMetaValue: function(isChecked) {
				dispatch('core/editor').editPost({
					[props.metaKey]: isChecked
				});
			}
		}
	}), withSelect(function(select, props) {
		return {
			metaValue: select('core/editor').getEditedPostAttribute(props.metaKey)
		}
	}))(function(props) {
		return el(CheckboxControl, {
			label: props.title,
			checked: props.metaValue,
			onChange: function(isChecked) {
				props.setMetaValue(isChecked);
			}
		});
	});

	registerPlugin(data.name, {
		icon: "none",
		render: function(props) {
			var controls = data.vars.map(function(item) {
				if (item.type == 'checkbox') {
					return el(MetaCheckboxControl, {
						metaKey: item.name,
						title: item.help
					});
				} else if (item.type == 'textarea') {
					return el(MetaTextareaControl, {
						metaKey: item.name,
						title: item.label
					});
				} else {
					return el(MetaTextControl, {
						metaKey: item.name,
						title: item.label
					});
				}
			});

			return el(PluginDocumentSettingPanel, {
				name: "custom-panel",
				title: data.title,
				className: "custom-panel",
			}, controls);
		},
	});
}
