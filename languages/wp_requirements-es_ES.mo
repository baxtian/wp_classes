��          D      l       �   "   �      �   U   �   +     <  >      {     �  o   �  B                             %s has been activated succesfully. %s requires %s. %s requires %s. Use this link to automatically <a href='%s'>install requirements</a>. Couldn't install %s. Pleas try it manually. Project-Id-Version: wp_requirements
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-10 16:40-0500
Last-Translator: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language-Team: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;esc_attr_e;esc_attr__
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: src
 %s se ha activado correctamente. %s requiere %s. %s requiere %s. Use el siguiente enlace para <a href='%s'>instalar los requerimientos</a> en forma automática. No se ha podido instalar %s. Por favor inténtelo en forma manual. 