��          �      �       H     I  U   R  Y   �  b        e     s  
     �   �     X     ]  �   u     C     S  Y  k  
   �  r   �  l   C  |   �     -     A     T  �   f     	     	  �   6	     
     -
            	                                                 
    Finished For reference you can export a CSV file with all %s using <a href='%s'>this link</a>. For reference you can export a CSV file with all movies using <a href='%s'>this link</a>. If the import time of a page takes longer than expected, try again until you get the final result. Import Movies New movies: New terms: Please select the file with the information and click on <b>%s</b>. The system will inform each process that is executed during the import and in the end will expose information with the imported elements. Step This is not a CSV file. This process imports a new record for each row of the CSV file and uses the data in the first row to determine the field corresponding to each column. Remember to add at least the <b>post_title</b> column. Updated movies: You must upload a file. Project-Id-Version: wp_importer
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-10 16:25-0500
Last-Translator: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language-Team: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;esc_attr_e;esc_attr__
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: src
X-Poedit-SearchPath-1: templates
 Finalizado Para referencia, puede exportar un archivo CSV con toda la información de %s usando <a href='%s'>este enlace</a>. Para referencia, puede exportar un archivo CSV con todas las películas usando <a href='%s'>este enlace</a>. Si el tiempo de importación de una página demora más de lo esperado, intente nuevamente hasta obtener el resultado final. Importar películas Películas nuevas: Nuevos términos: Seleccione el archivo con la información y haga clic en <b>%s</b>. El sistema informará cada paso ejecutado y al final expondrá información sobre los elementos importados. Paso Este no es un archivo CSV. Este proceso importa un nuevo registro para cada fila del archivo CSV y utiliza los datos en la primera fila para determinar el campo correspondiente a cada columna. Recuerde agregar al menos la columna <b>post_title</b>. Películas actualizadas: Debe subir un archivo. 