<?php
namespace Baxtian;

use \Timber as Timber;

define('WP_IMPORTER_V', '0.0.1');

if(!class_exists('WP_Importer')) {

	/**
	 * Clase base para los Importadores
	 */
	class WP_Importer
	{
		// Nombre estructura
		protected $tipo;
		protected $tipos;
		protected $accion;

		// Lista de campos
		protected $campos;

		// Lista de acciones
		protected $acciones;

		// Mensaje de publicación
		protected $textos;

		/**
		 * Constructor del importador
		 */
		protected function __construct()
		{
			// Solo por referencia
		}

		/**
		 * Inicializar el enlace del importador y los ajax para exportar e importar
		 */
		public function init() {
			add_action('admin_init', [$this, 'admin_init']);
			add_action('admin_init', [$this, 'i18n']);
			add_action('wp_ajax_export_' . $this->tipos, [$this, 'export']);
			add_action('wp_ajax_importer_' . $this->tipos, [$this, 'ajax']);
		}

		/**
		 * Importar el archivo de traducción
		 */
		public function i18n() {
			$domain = 'wp_importer';
			$locale = apply_filters( 'plugin_locale', determine_locale(), $domain );
		    $mofile = dirname(__DIR__) . '/languages/' . $domain . '-' . $locale . '.mo';

		 	load_textdomain( $domain, $mofile );
		}

		/**
		 * Incluir en el menú de importación
		 */
		public function admin_init()
		{
			register_importer($this->accion, $this->textos['import_button'], $this->textos['description'], [$this, 'page']);
		}

		/**
		 * A partir de un texto que puede o no estar separado por comas determina el
		 * term_id de cada uno al mismo tiempo que irá creando los términos que no existan.
		 * Retorna un arreglo con los identificadores de cada término.
		 * @param  string  $taxonomia Taxonomía de los términos.
		 * @param  string  $texto     Nombre del término.
		 * @param  boolean $separador Indicar si se debe usar ',' para separar el texto en
		 *                            varios términos
		 * @return array              Arreglo con los identificadores de los términos
		 */
		private function get_term($taxonomia, $texto, $separador = true)
		{
			// Inicializar la respuesta
			$answer = array();

			// Si estamos usando la coma como separador
			if ($separador) {
				// Extraer todos los términos en el texto (lista separada por coma) y retirar
				// los espacios al inicio y al fin
				$terms = array_map('trim', explode(',', $texto));
			} else {
				// Retirar los espacios al inicio y al fin
				$terms = array(trim($texto));
			}

			// Crear el arreglo de la taxonomía si no está en la lista de taxonomías
			if (!isset($this->taxonomia[$taxonomia])) {
				$this->taxonomia[$taxonomia] = array();
			}

			// Recorrer la lista de términos
			foreach ($terms as $item) {
				// Estandarizar los términos
				// Mayúscula en la primera letra de cada palabra
				$item = ucwords(trim($item));
				// Si aun no tenemos registro de ese término entonces hay que crearlo
				if (empty($this->taxonomia[$taxonomia][$item])) {
					//Buscar ese término en los registros de la taxonomía correspondiente
					$term = get_term_by('name', $item, $taxonomia);

					// ¿Existe registro de ese término en la taxonomía?
					if ($term) {
						// Si encontramos el término registrarlo en el arreglo de taxonomías
						// Usar como llave el texto del nombre
						$this->taxonomia[$taxonomia][$item] = $term->term_id;
					} else {
						// Si no existe registro del término entonces se crea el término en la taxonomía
						$term = wp_insert_term($item, $taxonomia);
						// Guardar el registro en el arreglod e taxonomías y usar el texto del nombre como llave
						$this->taxonomia[$taxonomia][$item] = $term['term_id'];
						// Guardar registro de la acción
						$this->acciones['terminos_nuevos']++;
					}
				}

				// Incluir en respuesta lo que tenemos en el arreglo de taxonomía para esa llave
				$answer[] = $this->taxonomia[$taxonomia][$item];
			}

			// Retornar respuesta
			return $answer;
		}

		/**
		 * Llamado ajax para atender la importación
		 * @return string JSON con los datos de importación
		 */
		public function ajax()
		{
			// Por defecto no habra siguiente paso de importación
			$answer = array(
				'siguiente' => 0
			);

			// Si envía el número de paso y hay archivo a importar
			if (isset($_GET['step']) && isset($_GET['filename'])) {
				$filename = sanitize_text_field($_GET['filename']);
				$step = sanitize_text_field($_GET['step']);
				$answer = $this->import_file($filename, $step);
			}

			echo json_encode($answer);
			wp_die();
		}

		/**
		 * Función para importar el archivo. Para evitar que se supere el tiempo del
		 * servidor se hace la importación por grupos.
		 * Por defecto estos grupos son de 50 elementos
		 * @param  string  $filename Archivo a importar
		 * @param  integer $step     Grupo que se importará.
		 * @param  integer $items    Número de items por grupo.
		 * @return array             Datos de la importación.
		 *                           array(
		 *                           	primero: posición en lista del primer item importado de este grupo
		 *                           	ultimo: posición en lista del último item importado de este grupo
		 *                           	siguiente: número del siguiente grupo a importar.
		 *                           			   False para indicar que no hay más grupos
		 *                           	mensajes: array(
		 *                           		mensaje_publicacion: Mensaje con los datos importados
		 *                           		nuevas: cantidad de nuevos items agregados.
		 *                           		modificadas: cantidad de items modificados.
		 *                           		terminos_nuevos: número de términos que se crearon
		 *                           	)
		 *                           )
		 */
		private function import_file($filename, $step = 1, $items = 50)
		{
			global $wpdb;

			// Lee todas las filas del archivo y las almacena en un arreglo, usando
			// la primera fila como llave para el valor de cada columna
			$csv = array_map('str_getcsv', file($filename));
			array_walk($csv, function (&$a) use ($csv) {
				$a = array_combine($csv[0], $a);
			});
			// Eliminar la primera fila por tratarse de la fila con los nombres de las columnas.
			array_shift($csv);

			// Total a importar
			$total_items = count($csv);

			// Sacar solo los que vamos a importar en este paso
			$data = array_slice($csv, ($step - 1) * $items, $items);

			// ¿Quedan aun por importar?
			$primero = (($step - 1) * $items);
			$ultimo = (($step - 1) * $items) + $items;
			$pendientes = ($ultimo >= $total_items) ? false : true;

			// Recorrer el arreglo de datos
			foreach ($data as $line) {


				// Usar esta primera para determinar si se incluyen todos los datos
				// que requerimos para crear los registros.

				// Solo intentar la importación si tenemos disponibilidad del título
				if (isset($line['post_title']) && !empty($line['post_title'])) {
					// ¿Trae ID este archivo y no está vacío? Entonces usar
					if (isset($line['id']) && !empty($line['id'])) {
						// Determinar que el ID sí existe y corresponde
						$post_id = false;
						$sql = "SELECT ID
							FROM $wpdb->posts
							WHERE post_title = '%s'
								AND post_type = '$this->tipo'
								AND ID = '%d'
								AND post_status = 'publish'";
						$post_id = $wpdb->get_var($wpdb->prepare($sql, $line['post_title'], $line['id']));
					} else {
						// Buscar el ID de una película que ya tengamos registrada con ese nombre
						$post_id = false;
						$sql = "SELECT ID
							FROM $wpdb->posts
							WHERE post_title = '%s'
								AND post_type = '$this->tipo'
								AND post_status = 'publish'";
						$post_id = $wpdb->get_var($wpdb->prepare($sql, $line['post_title']));
					}

					// ¿Hay un id?
					if (!$post_id) {
						// Si no hay un ID para una película con ese nombre, entonces crearla
						// Crear el registro con el título, de tipo 'película' y en estado 'publicado'.
						$args = array(
							'post_title' => $line['post_title'],
							'post_type' => $this->tipo,
							'post_status' => 'publish',
						);
						// Crear la película y guardar el id
						$post_id = wp_insert_post($args);

						// Registrar la acción
						$this->acciones['nuevas']++;
					} else {
						// Si tenemos un ID entonces es porque la película ya existe.
						// Por lo tanto usaremos ese ID para las acciones sobre esa película

						// Registrar la acción
						$this->acciones['modificadas']++;
					}

					// Ir creando el elemento de my_post por si hay un cambio en los datos de la estructura
					$my_post = array();

					// Recorrer el arreglo de campos y guardar según el tipo
					foreach ($this->campos as $campo) {
						$name = $campo['name'];
						switch ($campo['type']) {
							case 'field':
								// Solo continuar si el arreglo de datos dispone de esta variable
								if (isset($line[$name])) {
									// Los campos de tipo field se guardarán en conjunto al terminar de leer
									// las columnas de esta fila
									$my_post[$name] = $line[$name];
								}
								break;
							case 'term':
								// Solo continuar si el arreglo de datos dispone de esta variable
								if (isset($line[$name])) {
									// Determinar si es una taxonomía que incluye comas en su texto
									$usar_separador = in_array($name, array('taxonmia_que_usa_coma')) ? false : true;

									// Obtener arreglo con ID de los términos en la variable
									$terms = $this->get_term($name, $line[$name], $usar_separador);

									// Reemplazar los términos de esta entrada con los obtenidos
									// desde el arreglo de datos
									wp_set_post_terms($post_id, $terms, $name);
								}
								break;
							case 'postmeta':
								// Solo continuar si el arreglo de datos dispone de esta variable
								if (isset($line[$name])) {
									// Almacenar directamente como postmeta
									update_post_meta($post_id, $name, sanitize_text_field($line[$name]));
								}
								break;
							case 'postmeta_bool':
								// Solo continuar si el arreglo de datos dispone de esta variable
								if (isset($line[$name])) {
									// Convertir en booleano (1 o 0) y almacenar en postmeta
									update_post_meta($post_id, $name, (bool)($line[$name]));
								}
								break;
						}
					}

					// Hay datos en el my_post?
					if (count($my_post) > 0) {
						$my_post['ID'] = $post_id;
						wp_update_post($my_post);
					}
				}
			}
			$answer = array(
				'acciones' => $this->acciones
			);
			$answer['primero'] = $primero;
			$answer['ultimo'] = $ultimo;
			$answer['siguiente'] = ($pendientes) ? $step+1 : 0;

			$answer['mensaje'] = sprintf(
				$this->textos['mensaje_publicacion'],
				$this->acciones['nuevas'],
				$this->acciones['modificadas'],
				$this->acciones['terminos_nuevos']
			);

			// Si no hay siguiente, borrar archivo
			if (!$pendientes) {
				unlink($filename);
			}

			// Retornar acciones realizadas
			return $answer;
		}

		/**
		 * Renderizado de página de importación
		 */
		public function page()
		{
			// Directorios
			$url = dirname(plugin_dir_url(__FILE__)) . '/';
			$path = dirname(plugin_dir_path(__FILE__)) . '/languages/';

			// Scripts y estilos
			wp_register_script('importer', $url . "assets/scripts/importer.js", array('jquery'), WP_IMPORTER_V);
			wp_localize_script('importer', 'importer_var', array( 'step' => __('Step', 'wp_importer'), 'finished' => __('Finished', 'wp_importer') ));
			wp_enqueue_script('importer');

			wp_register_style('importer', $url . "assets/css/importer.css", array(), WP_IMPORTER_V);
			wp_enqueue_style('importer');

			// Datos
			$args = array();

			$args['importar'] = false;
			$tpl = "importers/form.twig";

			// ¿Hay un archivo para importar?
			if (isset($_FILES['import_file']) && $_FILES['import_file']['size'] > 0) {
				// Al parecer estamos importando
				$args['importar'] = true;
				$uploadOk = 1;

				// Detectar la extensión del archivo
				$fileType = pathinfo(basename($_FILES["import_file"]["name"]), PATHINFO_EXTENSION);

				// Solo permitir archivos de extensión csv
				if ($fileType != "csv") {
					$args['mensaje'] = __("This is not a CSV file.", 'wp_importer');
					$args['importar'] = false;
					$uploadOk = 0;
				}
			} elseif (isset($_FILES['import_file']) && $_FILES['import_file']['size'] == 0) {
				// Se envió un archivo pero está vacío
				$args['mensaje'] = __("You must upload a file.", 'wp_importer');
				$args['importar'] = false;
			}

			// Si las pruebas sobre el archivo fueron positivas, aplicar la importación
			if ($args['importar']) {
				// Datos para usar la plantilla
				$tpl = "importers/importer.twig";

				// Guardar archivio temporal
				$tempfile = wp_tempnam('importer_'. $this->tipos);
				move_uploaded_file($_FILES["import_file"]["tmp_name"], $tempfile);
				$args['filename'] = $tempfile;
			}

			// Datos para el archivo de referencia que estará disponible para que los usuarios lo descarguen.
			$args['enlace_exportar'] = admin_url('admin-ajax.php?action=export_' . $this->tipos);

			// Spinner
			$args['spinner'] = $url . 'assets/images/spinner.svg';

			// Textos
			$args['texto'] = $this->textos;

			// Textos
			$args['action'] = $this->accion;

			// Twig
			$this->render($tpl, $args);
		}

		/**
		 * Crea el archivo de CSV con los datos
		 */
		public function export()
		{
			// Encabezados para descarga
			header("Content-Type: text/csv");
			header("Content-Disposition: attachment; filename={$this->tipos}.csv");
			header("Cache-Control: no-cache, no-store, must-revalidate");
			header("Pragma: no-cache");
			header("Expires: 0");

			// Datos de las películas
			$posts = \Timber::get_posts(
				array(
					'post_type' => $this->tipo,
					'nopaging' => true
				)
			);
			// Inicializar arreglo de datos
			$data = array();

			// Quitar el ID si no hay datos
			if (count($posts) == 0) {
				array_shift($this->campos);
			}

			// Crear fila de nombres
			$first_row = array();
			foreach ($this->campos as $item) {
				$first_row[] = $item['name'];
			}
			$data[] = $first_row;

			// Recorrer las filas
			foreach ($posts as $post) {
				$row = array();
				// Recorrer los campos y agregar el elemento al arreglo de la fila
				foreach ($this->campos as $item) {
					$field_name = $item['name'];
					if ($item['type'] == 'term') { // Si es una taxonomía
						$terms = $post->terms($field_name);
						array_walk($terms, function (&$a) use ($terms) {
							$a = $a->name;
						});
						$row[] = implode(', ', $terms);
					} elseif ($item['type'] == 'postmeta') { // Si es un postmeta
						$row[] = $post->$field_name;
					} elseif ($item['type'] == 'postmeta_bool') { // Si es un postmeta
						$row[] = ($post->$field_name) ? 1 : 0; // Y además es booleano
					} else {
						$row[] = $post->$field_name;
					}
				}
				$data[] = $row;
			}

			// Crear archivo CSV
			$output = fopen('php://memory', 'r+');
			foreach ($data as $item) {
				fputcsv($output, $item);
			}
			rewind($output);

			// Imprimir archivo CSV
			echo stream_get_contents($output);

			// Cerrar el administrador del archivo
			fclose($output);

			// Cerrar llamado AJAX.
			wp_die();
		}

		/**
		 * Función para renderizar una plantilla.
		 * Requiere tener activo el plugin Timber.
		 * Lea la documentación de Timber en http://timber.github.io/timber/
		 * Timber está basado en Twig.
		 * Lee la documentación de Twig en https://twig.sensiolabs.org/doc/2.x/
		 * @param  string  $tpl   Plantilla twig a ser usada
		 * @param  array   $args  Arregloc on los argumentos a ser enviados a la plantilla
		 * @param  boolean $echo  Si desea capturar la salida del render use el parámetro echo. Si es false devolverá
		 * 						  la información directamente en vez de imprimirla. Por defecto es true.
		 * @return boolean|string Si echo es true indica si pudo o no renderizar la plantilla. Si es false retornará la
		 * 						  cadena con el texto renderizado o false si no pudo ejecutar la acción
		 */
		private function render($tpl, $args, $echo = true) : string
		{
			// Declarar el directorio de plantillas twig (templates o views)
			Timber::$dirname = array('../templates', '../views');

			// Obtener contexto para timber
			$context = Timber::get_context();

			// Datos del plugin
			$context['plugin'] = array( 'directory' => plugin_dir_url(__FILE__) );

			// Render
			if ($echo) {
				return Timber::render($tpl, array_merge($context, $args));
			}
			// ¿o devolver la cadena?
			return Timber::fetch($tpl, array_merge($context, $args));
		}
	}
}
