<?php
namespace Baxtian;

define('WP_POSTMETA_V', '0.0.1');

if(!class_exists('WP_PostMeta')) {

	/**
	 * Clase base para los Meta de las estructuras
	 */
	class WP_PostMeta
	{
		// Datos generales
		protected $prefix;
		protected $variables = array();
		protected $post_types = array();
		protected $display = 'side'; //side, block, box
		protected $column_position = -1;

		// Datos que se calcularán
		protected $quickedit_vars = array();
		protected $column_vars = array();
		protected $sortable_vars = array();

		/**
		 * Constructor de PostMeta
		 */
		protected function __construct()
		{
			add_action('admin_enqueue_scripts', [$this, 'bulk_scripts'], 9);
		}

		/**
		 * Generar los datos que deben ser calculados, declarar la funcionalidad para el
		 * Rest API, declarar el uso de edición en bloque y edición rápida, y declarar las
		 * columnas para cada estructura con la que se vincule este PostMeta.
		 */
		protected function init()
		{
			// Crear el arreglo con las variables que serán editadas por quickedit
			foreach ($this->variables as $item) {
				if ($item['quickedit'] || ( isset($item['column']) && $item['column'] )) {
					$this->column_vars[$item['name']] = $item;

					if($item['quickedit']) {
						$this->quickedit_vars[$item['name']] = $item;
					}

					if(!isset($item['sortable']) || $item['sortable']) {
						$this->sortable_vars[$item['name']] = $item;
					}
				}
			}

			// Inicializar en el rest api las variables que son quick edit
			add_action('rest_api_init', [$this, 'api']);

			// Determinar variables y acciones para quickedit y bulkedit
			add_action('bulk_edit_custom_box', [$this, 'bulkedit'], 10, 2);
			add_action('quick_edit_custom_box', [$this, 'quickedit'], 10, 2);

			// Vincular el almacenamiento de la estructura con estas variables
			foreach( $this->post_types as $post_type ) {
				// Guardar
				add_action('save_post_' . $post_type , [$this, 'save']);

				// Determninar columnas para la vista lista de admninistrador
				add_filter('manage_' . $post_type . '_posts_columns', [$this, 'columns']);
				add_action('manage_' . $post_type . '_posts_custom_column', [$this, 'columns_content'], 10, 2);

				// Determinar ordenamiento
				add_filter('manage_edit-' . $post_type . '_sortable_columns', [$this, 'sortable_columns']);
				add_action('pre_get_posts', [$this, 'orderby_columns'], 1);
			}
		}

		/**
		 * Función para declarar los scripts de edición en bloque de es este TermMeta.
		 * @param  string $hook Gancho
		 */
		public function bulk_scripts($hook)
		{
			//Directorios
			$url = dirname(plugin_dir_url(__FILE__)) . '/';

			//Solo activar si estamos en un post_type para esta caja
			$screen = get_current_screen();
			if (!in_array($screen->post_type, $this->post_types)) {
				return;
			}

			//Registrar quickedit
			wp_register_script('postmeta_quick_edit', $url . "assets/scripts/quick_edit.js", array(), WP_POSTMETA_V);

			//Registrar postmeta-side
			wp_register_script('postmeta_side', $url . "assets/scripts/postmeta-side.js",  array("wp-i18n", "wp-blocks", "wp-edit-post", "wp-element", "wp-editor", "wp-components", "wp-data", "wp-plugins", "wp-edit-post"), WP_POSTMETA_V);

			//Registrar postmeta-block
			wp_register_script('postmeta_block', $url . "assets/scripts/postmeta-block.js", array( "wp-blocks", "wp-editor", "wp-i18n", "wp-element", "wp-components" ), WP_POSTMETA_V);
		}

		/**
		 * Generador del arreglo a ser enviado con register_rest_field
		 * @param  string $slug        Taxonomía
		 * @param  string $description Nombre del campo
		 * @return array               El arreglo requerido para ser llamado por register_rest_field
		 */
		private function api_args($slug, $description)
		{
			return array(
				'get_callback'    => function ($object, $field_name, $request) {
					return get_post_meta($object['id'], $field_name, true);
				},
				'update_callback' => function ($value, $object, $field_name) {
					return update_post_meta($object->ID, $field_name, wp_slash($value));
				},
				'schema'          => array($slug => $description),
			);
		}

		/**
		 * Registrar el meta term en WPJson.
		 * ToDo: ¿Usar si la estructura ya exisitía y no se ha declarado el schema para estos post meta?
		 */
		public function api()
		{
			foreach ($this->post_types as $post_type) {
				foreach ($this->variables as $item) {
					register_rest_field(
					$post_type,
					$item['name'],
					$this->api_args($item['name'], $item['label'])
				);
				}
			}
		}

		/**
		 * Función para agregar las columnas al quickedit
		 * @param  string $column_name Nombre de la columna a ser agregada
		 * @param  string $post_type   Nombre del tipo de estructura
		 */
		public function quickedit($column_name, $post_type)
		{
			// Recorrer la lista de términos asignado para quickedit
			foreach ($this->quickedit_vars as $key => $item) {
				// Si la columna coincide con el elemento que estamos analizando
				if ($item['name'] == $column_name) {
					// Renderizar los datos del item
					$args['element'] = $item;

					// Twig
					$this->render('partial/quickedit.twig', $args);

					// Agregar los datos de esta variable para el javascript
					add_filter('quickedit_vars', function ($quickedit_vars) {
						return array_merge($quickedit_vars, $this->quickedit_vars);
					});

					// Ya no necesitamos buscar más
					break;
				}
			}
		}

		/**
		 * Renderizar la columna en el editor de quickedit
		 * @param  string $column_name Nombre de la columna a ser agregada
		 * @param  string $post_type   Nombre del tipo de estructura
		 */
		public function bulkedit($column_name, $post_type)
		{
			// Recorrer la lista de términos asignado para quickedit
			$element = false;
			foreach ($this->quickedit_vars as $key => $item) {
				// Si la columna coincide con el elemento que estamos analizando
				if ($item['name'] == $column_name) {
					// Renderizar los datos del item
					$args['element'] = $item;

					// Twig
					$this->render('partial/bulkedit.twig', $args);

					// Ya no necesitamos buscar más
					break;
				}
			}
		}

		/**
		 * Guardar los datos de este PostMeta
		 * @param  int $post_id ID de la estructura que se está almacenando
		 */
		public function save($post_id)
		{
			global $pla_save_flag, $post;

			// Verificar que no sea una rutina de autoguardado.
			// Si es así, el formulario no ha sido enviado y por lo tanto cancelamos
			// el proceso.
			if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
				return;
			}

			// Revisar permisos del usuario
			if (!current_user_can('edit_post', $post_id)) {
				return;
			}

			// Almacenar variables

			// Si estamos editando desde la vista de edición
			// Como estamos usando javascript / rest, esta parte ya no se usa, pero se
			// deja acá para referencia
			if (isset($_POST['action']) && $_POST['action'] == 'editpost') {
				// Solo guardaremos cosas por POST si este es un metabox
				if($this->display == "box") {
					foreach ($this->variables as $item) {
						$key = $item['name'];
						// Obtener el valor de la variable desde el formulario
						if ($item['type'] == 'checkbox') {
							update_post_meta($post_id, $key, isset($_POST[$key]));
						} else {
							$value = apply_filters( 'postmeta_value', sanitize_text_field($_POST[$key]), $key, $this->prefix, $post_id );
							update_post_meta($post_id, $key, $value);
						}
					}
				}
			// Si estamos editando desde la vista de edición rápida
			// Esto se hace porque hay campos en el formulario de edición que no están en el de edición rápida
			} elseif (isset($_POST['action']) && $_POST['action'] == 'inline-save') {
				// Obtener el valor de la variable desde el formulario
				// Solo usar las variables quick_edit
				foreach ($this->quickedit_vars as $key => $item) {
					if ($item['type'] == 'checkbox') {
						update_post_meta($post_id, $key, isset($_POST[$key]));
					} else {
						update_post_meta($post_id, $key, sanitize_text_field($_POST[$key]));
					}
				}

				// Si estamos editando desde la edición en lote
			} elseif (isset($_GET['bulk_edit'])) {
				// Obtener el valor de la variable desde el formulario
				foreach ($this->quickedit_vars as $key => $item) {
					if ($item['type'] == 'checkbox') {
						$$key = ($_GET[$key] == 1);
					} else {
						$$key = sanitize_text_field($_GET[$key]);
					}
				}

				$lote = $_GET['post'];
				foreach ($lote as $post_id) {
					foreach ($this->quickedit_vars as $key => $item) {
						if ($item['type'] == 'checkbox') {
							// Solo aplicar si se declara algún cambio
							if ($_GET[$key] != -1) {
								update_post_meta($post_id, $key, ($$key == 1));
							}
						} else {
							// Solo aplicar si se declara algún cambio
							if (!empty($$key)) {
								update_post_meta($post_id, $key, $$key);
							}
						}
					}
				}
			}

			/********************/
			//Si hubo edición (sea desde vista de edición, desde edición rápida o desde edición en lote)
			//y es necesario editar el título
			/*if ((isset($_POST['action']) && in_array($_POST['action'], array('editpost', 'inline-save'))) || isset($_GET['bulk_edit'])) {
				//Si es de modificación en lote
				if (isset($_GET['bulk_edit'])) {
					$lote = $_GET['post'];
				} else { //Si s un único post
					$lote = array($post_id);
				}

				foreach ($lote as $aux_id) {
					//Asignar título
					if ($pla_save_flag == 0) {
						$pla_save_flag = 1;
						//Asignar título
						$post = get_post($aux_id);

						$my_post = array();
						$my_post['ID'] = $post_id;
						$my_post['post_title'] = sprintf("Titulo especial %s %d", $post_id, mt_rand(1111, 9999));
						$my_post['post_name'] =  '';

						wp_update_post($my_post);
						$pla_save_flag = 0;
					}
				}
			}*/
			/********************/

			return;
		}

		/**
		 * Determinar las columnas a ser visualizadas en el visor de la lista de cada estructura
		 * a la que fue vinculada este PostMeta
		 * @param  array $columns Lista de columnas que actualmente está asignada a la estructura
		 * @return array          Lista con las nuevas columnas
		 */
		public function columns($columns)
		{
			// Determinar las nuevas columnas
			$new_columns = array();
			foreach ($this->column_vars as $key => $item) {
				$new_columns[$key] = $item['label'];
			}

			// Determinar posición en que irá la columna
			$pos = $this->column_position;
			if( $this->column_position < 0) {
				$pos   = count($columns) + $this->column_position;
			}

			// Mezclar las columnas
			$columns = array_merge(
				array_slice($columns, 0, $pos),
				$new_columns,
				array_slice($columns, $pos)
			);

			// Retornar columnas
			return $columns;
		}

		/**'postmeta'
		 * Contenido a mostrar en cada columna
		 * @param  string $content Contenido actual
		 * @param  string $column  Slug de la columna
		 * @param  int    $term_id ID del term que se está visualizando
		 */
		public function columns_content($column, $post_id)
		{
			$vars = $this->column_vars;
			if(isset($vars[$column])) {
				$var = $vars[$column];
				switch ($var['type']) {
					case 'color':
						$data = get_post_meta($post_id, $column, true);
						if (!empty($data)) {
							printf('<div class="termmeta-color-block" data-%s="%2$s" style="background-color: %2$s;"></div>', $column, $data);
						}
						break;
					case 'image':
						$imagen_logo = get_post_meta($post_id, $column, true);
						$imagen = false;
						if ($imagen_logo) {
							$src = wp_get_attachment_image_src($imagen_logo, 'thumbnail');
							printf('<img class="termmeta-image-block" data-%s="%s" src="%s">', $column, $imagen_logo, $src[0]);
						}
						break;
					case 'checkbox':
						$content = (get_post_meta($post_id, $column, true)) ? "<i class='dashicons dashicons-yes' data-" . $column . "='1'></i>" : "<i class='dashicons dashicons-no-alt' data-" . $column . "='0'></i>";
						echo $content;
						break;
					default:
						$data = get_post_meta($post_id, $column, true);
						if (!empty($data)) {
							printf('<span data-%s="%2$s">%2$s</span>', $column, $data);
						}
						break;
				}
			}
		}

		/**
		 * Determinar los campos por los que se puede ordenar
		 * @param  array $post_columns Columnas por las que se puede ordenar actualmente
		 * @return array               Arreglo de columnas incluyendo las que se defineron como ordenables en este PostMeta
		 */
		public function sortable_columns($post_columns)
		{
			// Determinar las columnas ordenables
			$new_columns = array();
			foreach ($this->sortable_vars as $key => $item) {
				$new_columns[$key] = $key;
			}

			// Agregar los campos propios de esta estructura y que estarán
			// disponibles para el ordenamiento de la lista
			$post_columns = array_merge(
				$post_columns,
				$new_columns
			);

			// Retornar nuevas columnas ordenables
			return $post_columns;
		}

		/**
		 * Ordenar según los campos propios de este TermMeta
		 * @param  WP_Query $query Instancia de consulta
		 */
		public function orderby_columns($query)
		{
			// Determinar cuál va a ser el termmeta por el cuál se está ordenando
			$orderby_items = array();
			foreach ($this->sortable_vars as $key => $item) {
				$orderby_items[] = $item['name'];
			}

			// Correr solo en el ordenamiento principal y si está solicitando 'orderby'
			if ($query->is_main_query() && ($orderby = $query->get('orderby'))) {
				if (in_array($orderby, $orderby_items)) {
					// Determinar el meta_key para la búsqueda
					$query->set('meta_key', $orderby);
					// Determinar que el tipo de ordenamiento se hará por la variable
					// asignada en meta_key. Por usar 'meta_value' el ordenamiento es
					// alfabético. Para ordenamiento numérico se usa 'meta_value_num'
					$query->set('orderby', 'meta_value');
				}
			}
		}

		/**
		 * Función para renderizar una plantilla.
		 * Requiere tener activo el plugin Timber.
		 * Lea la documentación de Timber en http://timber.github.io/timber/
		 * Timber está basado en Twig.
		 * Lee la documentación de Twig en https://twig.sensiolabs.org/doc/2.x/
		 * @param  string  $tpl   Plantilla twig a ser usada
		 * @param  array   $args  Arregloc on los argumentos a ser enviados a la plantilla
		 * @param  boolean $echo  Si desea capturar la salida del render use el parámetro echo. Si es false devolverá
		 * 						  la información directamente en vez de imprimirla. Por defecto es true.
		 * @return boolean|string Si echo es true indica si pudo o no renderizar la plantilla. Si es false retornará la
		 * 						  cadena con el texto renderizado o false si no pudo ejecutar la acción
		 */
		private function render($tpl, $args, $echo = true) : string
		{
			// Declarar el directorio de plantillas twig (templates o views)
			Timber::$dirname = array('../templates', '../views');

			// Obtener contexto para timber
			$context = Timber::get_context();

			// Datos del plugin
			$context['plugin'] = array( 'directory' => plugin_dir_url(__FILE__) );

			// Render
			if ($echo) {
				return Timber::render($tpl, array_merge($context, $args));
			}
			// ¿o devolver la cadena?
			return Timber::fetch($tpl, array_merge($context, $args));
		}
	}
}
