<?php
namespace Baxtian;

if(!class_exists('WP_Block')) {

	/**
	 * Clase padre de bloques
	 */
	class WP_Block
	{
		// Variables
		protected $blockname;
		protected $classname;
		protected $dependencies;
		protected $dirs;

		/**
		 * Crea la generación del bloque usando los scripts y estilos compilados en el
		 * directorio build/[$this->classname]
		 * Buscará los siguientes archivos:
		 * 		block.js - script del bloque
		 * 		editor.css - estilo exclusivo del backend
		 * 		frontend.js - scripts del frontend
		 * 		style.css - estilos de frontend y backend
		 */
		protected function register_block_type() : void {
			// Si aun no están registrados los datos o no está activo Gutenberg no continuar
			$a = function_exists('register_block_type');
			$b = $this->blockname;
			$c = $this->classname;

			if( empty($this->blockname) || empty($this->classname) || ! function_exists('register_block_type') ) {
				return;
			}

			// Valores por defecto para las dependencias
			$dependencies = shortcode_atts(
				array(
					'block' => array(),
					'editor' => array(),
					'frontend' => array(),
					'style' => array(),
				),
				$this->dependencies
			);

			// Microname
			$arr = explode('/', $this->blockname);
			$microname = array_pop($arr);

			// Directorios
			$dirs = shortcode_atts(
				array(
					'url' => array(),
					'path' => array(),
					'lang_path' => array()
				),
				$this->dirs
			);

			// Atributos
			$atts = array();

			// Script del bloque, traducciones y localización
			// BLOCK
			if(file_exists($dirs['path'] . 'block.asset.php')) {
				$atts['editor_script'] = $microname;
				$asset_file = include( $dirs['path'] . 'block.asset.php' );

				// Dependencias
				$_dependencies = array_unique( array_merge($asset_file['dependencies'], $dependencies['block']) );

				wp_register_script($microname, $dirs['url'] . "block.js", $_dependencies, $asset_file['version'], true);
				wp_set_script_translations($microname, PLA_TAX_D, $dirs['lang_path']);
				wp_localize_script($microname, PLA_TAX_P . '_blocks_var', array( 'prefix' => PLA_TAX_P, 'domain' => PLA_TAX_D));
			}

			// Estilo solo en backend
			// EDITOR
			if(file_exists($dirs['path'] . 'editor.asset.php')) {
				$atts['editor_style'] = $microname.'-editor';
				$asset_file = include( $dirs['path'] . 'editor.asset.php');

				// Dependencias
				$_dependencies = array_unique( array_merge(array(), $dependencies['editor']) );

				wp_register_style($microname.'-editor', $dirs['url'] . "editor.css", $_dependencies, $asset_file['version']);
			}

			// Script en el frontend
			// FRONTEND
			if(file_exists($dirs['path'] . 'frontend.asset.php')) {
				$atts['script'] = $microname.'-script';
				$asset_file = include( $dirs['path'] . 'frontend.asset.php' );

				// Dependencias
				$_dependencies = array_unique( array_merge($asset_file['dependencies'], $dependencies['frontend']) );

				wp_register_script($microname.'-script', $dirs['url'] . "frontend.js", $_dependencies, $asset_file['version'], true);
			}

			// Estilo en frontend y backend
			// STYLE
			if(file_exists($dirs['path'] . 'style.asset.php')) {
				$atts['style'] = $microname.'-style';
				$asset_file = include( $dirs['path'] . 'style.asset.php');

				// Dependencias
				$_dependencies = array_unique( array_merge(array(), $dependencies['style']) );

				wp_register_style($microname.'-style', $dirs['url'] . "style.css", $_dependencies, $asset_file['version']);
			}

			// ¿Existe una función de render?
			if(method_exists($this, 'render')) {
				$atts['render_callback'] = [$this, 'render'];
			}

			register_block_type(
				$this->blockname,
				$atts
			);
		}
	}
}
